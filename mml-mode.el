;; 
;; to install this mode, copy this file to ~/.emacs.d/lisp/mml-mode.el
;; and add the following lines to your init.el file:
;;     (add-to-list 'load-path "~/.emacs.d/lisp/")
;;     (load "mml-mode.el")
;; open an mml-file and activate mode with
;; ALT+X mml-mode RET
;;

;; helpfull links:
;; http://www.atarismwc.com/Puzzle/Puzzle Baserom (working)/AddmusicK_1.0.6/readme_files/syntax_reference.html
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html
;; https://www.emacswiki.org/emacs/RegularExpression
;; http://ergoemacs.org/emacs/elisp_regex.html

(setq mml-highlights
      '(
	(";.*" . font-lock-comment-face)
	("\".+?\"" . font-lock-string-face)
	("#samples\\|#default\\|#optimized\\|#instruments\\|#pad\\|#spc\\|#author\\|#game\\|#comment\\|#title\\|#path\\|#halvetempo\\|#option\\|#define\\|#undef\\|#ifdef\\|#ifndef\\|#if\\|#endif\\|#error\\|#amk\\|#[0-7]" . font-lock-function-name-face)
	("[wt@vyhqpn\*!][[:xdigit:]]\\{1,3\\}\\|o[1-6]" . font-lock-keyword-face)
	("\\$[[:xdigit:]]\\{2\\}" . font-lock-type-face)
	("&\\|?\\|/\\|=\\|\\[\\|\\]\\|\\^\\|<\\|>\\|{\\|}" . font-lock-constant-face)
       )
)

(define-derived-mode mml-mode text-mode
  "mml-mode (Music Macro Language)"
  "Mode for highlighting mml (Music Macro Language) sytax."
  (setq font-lock-defaults '(mml-highlights))
  )

; 
(defun convertandplay ()
  "Convert buffer to SPC"
  (defconst path-to-addmusick "C:/Program Files/AddmusicK/1.0.7/")
  (defconst path-to-spcplay "C:/Program Files/SPC700 PLAYER/")
  (interactive)
  (write-region (point-min) (point-max) (concat path-to-addmusick "/music/tmp.txt"))
  (setq default-directory path-to-addmusick)
  (call-process (concat path-to-addmusick "AddmusicK.exe") nil 0 nil "-norom" "tmp.txt")
  (sleep-for .5)
  (call-process (concat path-to-spcplay "spcplay.exe") nil 0 nil "SPCs/tmp.spc")
  (sleep-for .5)
  (delete-file "music/tmp.txt")
  (delete-file "SPCs/tmp.spc")
  )
(global-set-key (kbd "C-c C-c") 'convertandplay)

;; add the mode to the `features' list
(provide 'mml-mode)
