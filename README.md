# Music Macro Language for EMACS

Emacs mode to edit mml files

To install this mode, clone or copy the project to ~/.emacs.d/lisp/mml-mode

If you are in the HOME directory use the following command to clone:

    git clone https://gitlab.com/Nobody-86/music-macro-language-for-emacs.git ./.emacs.d/lisp/mml-mode

Then add the following lines to your init.el file:

    (add-to-list 'load-path "~/.emacs.d/lisp/")
    (load "mml-mode.el")
    (add-to-list 'auto-mode-alist '("\.mml$" . mml-mode))

The last line starts files with the extension .mml directly in mml-mode. Otherwise (e.g. for text files) must be started with

    ALT+X mml-mode

must be changed to the mml-mode.
